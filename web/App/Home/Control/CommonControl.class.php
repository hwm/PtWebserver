<?php
/**
 * Created by PhpStorm.
 * User: pantian
 * Date: 2015/5/6
 * Time: 14:22
 */

namespace App\Home\Control;


use PTPhp\Base\BaseControl;
use PTPhp\PT;

class CommonControl extends BaseControl{
    /**
     * 侧边菜单
     * @var array
     */
    protected $sideMenu = [
        'Index' => ['首页','glyphicon-home'],
        'webAdmin' => ['网站管理','glyphicon-home'],
        'Conf' => ['配置管理','glyphicon-home'],
        'Ftp' => ['FTP管理','glyphicon-home'],
        'Monitor'=>['系统监控','glyphicon-home'],
        'SysInfo'=>['服务器信息','glyphicon-home'],
    ];
    /**
     * @var \pt_server\pt_http_server
     */
    protected $pt_http_server = null;

    function __construct ()
    {
        parent::__construct();
        $this->assign('sideMenu',$this->sideMenu);
        $this->pt_http_server = PT::getPTServer();

    }

    /**
     * 设置标题
     *
     * @param $title
     */
    function setTitle ($title)
    {
        $this->variable['title'] = $title;
    }

    /**
     * 获取服务器配置数据
     *
     * @return mixed
     */
    function getPTHttpServerConfig ()
    {
        return $this->pt_http_server->getHttpServer()->getHttpConf();
    }
}