<?php
/**
 * Created by PhpStorm.
 * User: pantian
 * Date: 2015/4/13
 * Time: 22:23
 */

namespace lib;


class web {
    static $configFile = './conf/userConfig.php';

    /**
     * 保存配置
     *
     * @param array $data
     * @return bool
     */
    static function writeHttpConfig (array $data)
    {
        return self::write(self::$configFile, $data);
    }

    /**
     * 获取http 服务器配置
     * @return bool|mixed
     */
    static function getHttpConfig ()
    {
        if(!is_file(self::$configFile))return false;
        $str = file_get_contents(self::$configFile);
        if($str){
            return unserialize($str);
        }
        return false;
    }

    /**
     * 写文件
     *
     * @param $file
     * @param $data
     * @return bool
     */
    static function write ($file, $data)
    {
        $content = serialize($data);
        $f = fopen($file,'w+');
        if($f){
            $res=false;
            if(fwrite($f, $content))$res=true;
            fclose($f);
            return $res;
        }
        return false;
    }

}